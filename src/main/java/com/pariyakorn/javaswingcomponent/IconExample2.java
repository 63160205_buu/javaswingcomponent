/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.javaswingcomponent;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author acer
 */
public class IconExample2 {

    IconExample2() {
        JFrame f = new JFrame();
        Image icon = Toolkit.getDefaultToolkit().getImage("D:\\icon.png");
        f.setIconImage(icon);
        f.setLayout(null);
        f.setSize(200, 200);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new IconExample2();
    }
}
